package com.template;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mustache.web.MustacheViewResolver;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.mustache.java.LocalizationMessageInterceptor;

@Configuration
@EnableWebMvc
@ComponentScan({ "com.template.components" })
@PropertySources({ @PropertySource("classpath:application.properties"),
		@PropertySource(value = "file:${external.config}") })
public class TemplateApplicationConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	private MessageSource messageSource;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/static/");
	}

	@Bean
	public ViewResolver viewResolver() {
		MustacheViewResolver viewResolver = new MustacheViewResolver();
		viewResolver.setCache(true);
		return viewResolver;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
		registry.addInterceptor(localizationMessageInterceptor());
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
		interceptor.setParamName("language");
		return interceptor;
	}

	@Bean
	public LocalizationMessageInterceptor localizationMessageInterceptor() {
		LocalizationMessageInterceptor lmi = new LocalizationMessageInterceptor();
		lmi.setLocaleResolver(localeResolver());
		lmi.setMessageSource(messageSource);
		return lmi;

	}

	@Bean
	public SessionLocaleResolver localeResolver() {
		SessionLocaleResolver resolver = new SessionLocaleResolver();
		resolver.setDefaultLocale(Locale.ENGLISH);
		return resolver;
	}
}
