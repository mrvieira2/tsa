package com.template.components.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BasicController {
	@RequestMapping("/")
	public String root() {
		return "redirect:dashboard";
	}
	
	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("dashboard");
		modelAndView.addObject("sub", "Subtitulo");
		return modelAndView;
	}
	
	@RequestMapping("/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	@RequestMapping("/erro")
	public ModelAndView erro() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("erro");
		return modelAndView;
	}
	
	@RequestMapping("/json")
	public @ResponseBody String json() {
		return "{marcos:marcos}";
	}
}
