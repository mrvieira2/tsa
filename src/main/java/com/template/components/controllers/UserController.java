package com.template.components.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.template.components.services.UserServices;
import com.template.entities.User;

@Controller
public class UserController {
	
	@Autowired
	private UserServices userServices;
	
	@RequestMapping(path = "/users", method = RequestMethod.GET)
	public ModelAndView getUsers() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("users");
		return modelAndView;
	}
	
	@RequestMapping(path = "/users/page/{page}", method = RequestMethod.GET)
	public @ResponseBody Page<User> getUsersPage(@PathVariable Integer page) {
		return userServices.getUsers(page);
	}
}
