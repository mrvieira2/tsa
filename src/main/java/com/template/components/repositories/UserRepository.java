package com.template.components.repositories;

import java.io.Serializable;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.template.entities.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Serializable> {
	
	User findByLogin(String login);
	
}
