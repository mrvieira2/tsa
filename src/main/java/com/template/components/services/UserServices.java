package com.template.components.services;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.template.components.repositories.UserRepository;
import com.template.entities.User;

@Service
public class UserServices {
	@Autowired
	private UserRepository userRepository;
	
	
	@PostConstruct
	public void init() {
		User user = userRepository.findByLogin("admin");
		if (user == null) {
			user = new User();
			user.setLogin("admin");
			user.setPassword("admin");
			user.setEmail("admin@admin.com");
			user.setDateCreate(new Date());
			
			userRepository.save(user);
		}
	}
	
	public Page<User> getUsers(Integer page) {
		Sort sort = new Sort(Direction.ASC, "id");
		PageRequest request = new PageRequest(page, 10, sort);
		
		return userRepository.findAll(request);
	}

	public User findByLogin(String login) {
		return userRepository.findByLogin(login);
	}
}
