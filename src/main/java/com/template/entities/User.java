package com.template.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="users")
public class User implements Serializable {

	private static final long serialVersionUID = -1184796517617745604L;

	@Id
	@SequenceGenerator(name="seq_user_id", sequenceName="seq_user_id", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_user_id")
	@Column(columnDefinition = "bigint")
	private Long id;
	
	@Column(name="login", nullable=true)
	@Size(max=50)
	private String login;
	
	@Column(name="email", nullable=false)
	@Size(max=100)
	private String email;
	
	@Column(name="password", nullable=false)
	@Size(max=100)
	private String password;
	
	@Column(name="dt_create", nullable=false)
	private Date dateCreate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}
	
	
}
